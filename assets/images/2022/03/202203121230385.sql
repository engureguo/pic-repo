-- MySQL dump 10.13  Distrib 8.0.28, for macos11 (arm64)
--
-- Host: 127.0.0.1    Database: mvhr
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adjustsalary`
--

DROP TABLE IF EXISTS `adjustsalary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adjustsalary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL,
  `asDate` date DEFAULT NULL COMMENT '调薪日期',
  `beforeSalary` int DEFAULT NULL COMMENT '调前薪资',
  `afterSalary` int DEFAULT NULL COMMENT '调后薪资',
  `reason` varchar(255) DEFAULT NULL COMMENT '调薪原因',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `pid` (`eid`),
  CONSTRAINT `adjustsalary_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adjustsalary`
--

LOCK TABLES `adjustsalary` WRITE;
/*!40000 ALTER TABLE `adjustsalary` DISABLE KEYS */;
/*!40000 ALTER TABLE `adjustsalary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appraise`
--

DROP TABLE IF EXISTS `appraise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appraise` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL,
  `appDate` date DEFAULT NULL COMMENT '考评日期',
  `appResult` varchar(32) DEFAULT NULL COMMENT '考评结果',
  `appContent` varchar(255) DEFAULT NULL COMMENT '考评内容',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `pid` (`eid`),
  CONSTRAINT `appraise_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appraise`
--

LOCK TABLES `appraise` WRITE;
/*!40000 ALTER TABLE `appraise` DISABLE KEYS */;
/*!40000 ALTER TABLE `appraise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '部门名称',
  `parentId` int DEFAULT NULL,
  `depPath` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `isParent` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`id`, `name`, `parentId`, `depPath`, `enabled`, `isParent`) VALUES (1,'股东会',-1,'.1',1,1),(4,'董事会',1,'.1.4',1,1),(5,'总办',4,'.1.4.5',1,1),(8,'财务部',5,'.1.4.5.8',1,0),(78,'市场部',5,'.1.4.5.78',1,1),(81,'华北市场部',78,'.1.4.5.78.81',1,1),(82,'华南市场部',78,'.1.4.5.78.82',1,0),(85,'石家庄市场部',81,'.1.4.5.78.81.85',1,0),(86,'西北市场部',78,'.1.4.5.78.86',1,1),(87,'西安市场',86,'.1.4.5.78.86.87',1,1),(89,'莲湖区市场',87,'.1.4.5.78.86.87.89',1,0),(91,'技术部',5,'.1.4.5.91',1,0),(92,'运维部',5,'.1.4.5.92',1,1),(93,'运维1部',92,'.1.4.5.92.93',1,0),(94,'运维2部',92,'.1.4.5.92.94',1,0),(96,'bbb',1,'.1.96',1,1),(104,'111',96,'.1.96.104',1,0);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `name` varchar(10) DEFAULT NULL COMMENT '员工姓名',
  `gender` char(4) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `idCard` char(18) DEFAULT NULL COMMENT '身份证号',
  `wedlock` enum('已婚','未婚','离异') DEFAULT NULL COMMENT '婚姻状况',
  `nationId` int DEFAULT NULL COMMENT '民族',
  `nativePlace` varchar(20) DEFAULT NULL COMMENT '籍贯',
  `politicId` int DEFAULT NULL COMMENT '政治面貌',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `address` varchar(64) DEFAULT NULL COMMENT '联系地址',
  `departmentId` int DEFAULT NULL COMMENT '所属部门',
  `jobLevelId` int DEFAULT NULL COMMENT '职称ID',
  `posId` int DEFAULT NULL COMMENT '职位ID',
  `engageForm` varchar(8) DEFAULT NULL COMMENT '聘用形式',
  `tiptopDegree` enum('博士','硕士','本科','大专','高中','初中','小学','其他') DEFAULT NULL COMMENT '最高学历',
  `specialty` varchar(32) DEFAULT NULL COMMENT '所属专业',
  `school` varchar(32) DEFAULT NULL COMMENT '毕业院校',
  `beginDate` date DEFAULT NULL COMMENT '入职日期',
  `workState` enum('在职','离职') DEFAULT '在职' COMMENT '在职状态',
  `workID` char(8) DEFAULT NULL COMMENT '工号',
  `contractTerm` double DEFAULT NULL COMMENT '合同期限',
  `conversionTime` date DEFAULT NULL COMMENT '转正日期',
  `notWorkDate` date DEFAULT NULL COMMENT '离职日期',
  `beginContract` date DEFAULT NULL COMMENT '合同起始日期',
  `endContract` date DEFAULT NULL COMMENT '合同终止日期',
  `workAge` int DEFAULT NULL COMMENT '工龄',
  PRIMARY KEY (`id`),
  KEY `departmentId` (`departmentId`),
  KEY `jobId` (`jobLevelId`),
  KEY `dutyId` (`posId`),
  KEY `nationId` (`nationId`),
  KEY `politicId` (`politicId`),
  KEY `workID_key` (`workID`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `department` (`id`),
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`jobLevelId`) REFERENCES `joblevel` (`id`),
  CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`posId`) REFERENCES `position` (`id`),
  CONSTRAINT `employee_ibfk_4` FOREIGN KEY (`nationId`) REFERENCES `nation` (`id`),
  CONSTRAINT `employee_ibfk_5` FOREIGN KEY (`politicId`) REFERENCES `politicsstatus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1942 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`id`, `name`, `gender`, `birthday`, `idCard`, `wedlock`, `nationId`, `nativePlace`, `politicId`, `email`, `phone`, `address`, `departmentId`, `jobLevelId`, `posId`, `engageForm`, `tiptopDegree`, `specialty`, `school`, `beginDate`, `workState`, `workID`, `contractTerm`, `conversionTime`, `notWorkDate`, `beginContract`, `endContract`, `workAge`) VALUES (1,'江南一点雨','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',5,9,29,'劳务合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000001',2,'2018-04-01',NULL,'2018-01-01','2020-01-01',NULL),(2,'陈静','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',91,12,29,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000002',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(3,'赵琳浩','男','1993-03-04','610122199303041456','未婚',1,'陕西',3,'zhao@qq.com','15698887795','陕西省西安市莲湖区',91,12,33,'劳动合同','博士','电子工程','哈尔滨理工大学','2018-01-01','在职','00000003',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(4,'鹿存亮','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000004',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(5,'姚森','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','硕士','室内装修设计','西北大学','2017-01-02','在职','00000005',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(6,'云星','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000006',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(7,'贾旭明','男','1993-11-11','610122199311111234','已婚',1,'广东广州',4,'jiaxuming@qq.com','15644441234','广东省广州市天河区冼村路',78,15,33,'劳务合同','初中','通信工程','西北大学','2018-01-01','在职','00000007',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(8,'张黎明','男','1991-02-01','610144199102014569','已婚',1,'广东',6,'zhangliming@qq.com','18979994478','广东珠海',91,15,33,'劳动合同','高中','考古','清华大学','2018-01-01','在职','00000008',7,'2018-04-01',NULL,'2018-01-01','2025-01-30',NULL),(9,'薛磊','男','1992-07-01','610144199207017895','已婚',1,'陕西西安',13,'xuelei@qq.com','15648887741','西安市雁塔区',92,14,34,'劳动合同','初中','无','华胥中学','2018-01-01','在职','00000009',6,'2018-04-01',NULL,'2018-01-01','2024-01-17',NULL),(10,'张洁','女','1990-10-09','420177199010093652','未婚',1,'海南',5,'zhangjie@qq.com','13695557742','海口市美兰区',92,16,34,'劳动合同','高中','无','海南侨中','2018-01-01','在职','00000010',1,'2018-01-31',NULL,'2018-01-01','2019-01-01',NULL),(11,'江南一点雨2','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',91,9,29,'劳动合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000011',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(12,'陈静2','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',82,12,30,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000012',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(13,'赵琳浩2','男','1993-03-04','610122199303041456','未婚',1,'陕西',3,'zhao@qq.com','15698887795','陕西省西安市莲湖区',91,12,33,'劳动合同','博士','电子工程','哈尔滨理工大学','2018-01-01','在职','00000013',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(14,'鹿存亮2','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000014',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(15,'姚森2','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','初中','室内装修设计','西北大学','2017-01-02','在职','00000015',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(16,'云星2','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000016',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(17,'贾旭明2','男','1993-11-11','610122199311111234','已婚',1,'广东广州',4,'jiaxuming@qq.com','15644441234','广东省广州市天河区冼村路',78,15,33,'劳务合同','初中','通信工程','西北大学','2018-01-01','在职','00000017',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(18,'王一亭','男','1991-02-01','610144199102014569','已婚',1,'广东',6,'zhangliming@qq.com','18979994478','广东珠海',91,15,33,'劳动合同','高中','考古','清华大学','2018-01-01','在职','00000018',7,'2018-04-01',NULL,'2018-01-01','2025-01-30',NULL),(19,'薛磊2','男','1992-07-01','610144199207017895','已婚',1,'陕西西安',13,'xuelei@qq.com','15648887741','西安市雁塔区',92,14,34,'劳动合同','初中','无','华胥中学','2018-01-01','在职','00000019',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(20,'张洁2','女','1990-10-09','420177199010093652','未婚',1,'海南',5,'zhangjie@qq.com','13695557742','海口市美兰区',92,16,34,'劳动合同','高中','无','海南侨中','2018-01-01','在职','00000020',1,'2018-01-31',NULL,'2018-01-01','2019-01-01',NULL),(21,'江南一点雨3','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',8,9,29,'劳动合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000021',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(22,'陈静3','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',82,12,30,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000022',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(24,'鹿存亮3','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000024',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(25,'姚森3','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','初中','室内装修设计','西北大学','2017-01-02','在职','00000025',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(26,'云星3','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000026',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(27,'贾旭明3','男','1993-11-11','610122199311111234','已婚',1,'广东广州',4,'jiaxuming@qq.com','15644441234','广东省广州市天河区冼村路',78,15,33,'劳务合同','初中','通信工程','西北大学','2018-01-01','在职','00000027',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(28,'张黎明3','男','1991-02-01','610144199102014569','已婚',1,'广东',6,'zhangliming@qq.com','18979994478','广东珠海',91,15,33,'劳动合同','高中','考古','清华大学','2018-01-01','在职','00000028',7,'2018-04-01',NULL,'2018-01-01','2025-01-30',NULL),(29,'薛磊3','男','1992-07-01','610144199207017895','已婚',1,'陕西西安',13,'xuelei@qq.com','15648887741','西安市雁塔区',92,14,34,'劳动合同','初中','无','华胥中学','2018-01-01','在职','00000029',6,'2018-04-01',NULL,'2018-01-01','2024-01-17',NULL),(31,'江南一点雨4','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',8,9,29,'劳动合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000031',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(32,'陈静4','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',82,12,30,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000032',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(33,'赵琳浩4','男','1993-03-04','610122199303041456','未婚',1,'陕西',3,'zhao@qq.com','15698887795','陕西省西安市莲湖区',91,12,33,'劳动合同','博士','电子工程','哈尔滨理工大学','2018-01-01','在职','00000033',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(34,'鹿存亮4','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000034',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(35,'姚森4','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','初中','室内装修设计','西北大学','2017-01-02','在职','00000035',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(36,'云星4','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000036',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(37,'贾旭明4','男','1993-11-11','610122199311111234','已婚',1,'广东广州',4,'jiaxuming@qq.com','15644441234','广东省广州市天河区冼村路',78,15,33,'劳务合同','初中','通信工程','西北大学','2018-01-01','在职','00000037',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(38,'张黎明2','男','1991-02-01','610144199102014569','已婚',1,'广东',6,'zhangliming@qq.com','18979994478','广东珠海',91,15,33,'劳动合同','高中','考古','清华大学','2018-01-01','在职','00000038',7,'2018-04-01',NULL,'2018-01-01','2025-01-30',NULL),(39,'薛磊4','男','1992-07-01','610144199207017895','已婚',1,'陕西西安',13,'xuelei@qq.com','15648887741','西安市雁塔区',92,14,34,'劳动合同','初中','无','华胥中学','2018-01-01','在职','00000039',6,'2018-04-01',NULL,'2018-01-01','2024-01-17',NULL),(40,'张洁4','女','1990-10-09','420177199010093652','未婚',1,'海南',5,'zhangjie@qq.com','13695557742','海口市美兰区',92,16,34,'劳动合同','高中','无','海南侨中','2018-01-01','在职','00000040',1,'2018-01-31',NULL,'2018-01-01','2019-01-01',NULL),(41,'江南一点雨5','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',8,9,29,'劳动合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000041',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(42,'陈静5','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',82,12,30,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000042',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(43,'赵琳浩5','男','1993-03-04','610122199303041456','未婚',1,'陕西',3,'zhao@qq.com','15698887795','陕西省西安市莲湖区',91,12,33,'劳动合同','博士','电子工程','哈尔滨理工大学','2018-01-01','在职','00000043',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(44,'鹿存亮5','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000044',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(45,'姚森5','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','初中','室内装修设计','西北大学','2017-01-02','在职','00000045',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(46,'云星5','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000046',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(47,'贾旭明5','男','1993-11-11','610122199311111234','已婚',1,'广东广州',4,'jiaxuming@qq.com','15644441234','广东省广州市天河区冼村路',78,15,33,'劳务合同','初中','通信工程','西北大学','2018-01-01','在职','00000047',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL),(48,'张黎明5','男','1991-02-01','610144199102014569','已婚',1,'广东',6,'zhangliming@qq.com','18979994478','广东珠海',91,15,33,'劳动合同','高中','考古','清华大学','2018-01-01','在职','00000048',7,'2018-04-01',NULL,'2018-01-01','2025-01-30',NULL),(49,'薛磊5','男','1992-07-01','610144199207017895','已婚',1,'陕西西安',13,'xuelei@qq.com','15648887741','西安市雁塔区',92,14,34,'劳动合同','初中','无','华胥中学','2018-01-01','在职','00000049',6,'2018-04-01',NULL,'2018-01-01','2024-01-17',NULL),(50,'张洁5','女','1990-10-09','420177199010093652','未婚',1,'海南',5,'zhangjie@qq.com','13695557742','海口市美兰区',92,16,34,'劳动合同','高中','无','海南侨中','2018-01-01','在职','00000050',1,'2018-01-31',NULL,'2018-01-01','2019-01-01',NULL),(51,'江南一点雨6','男','1990-01-01','610122199001011256','已婚',1,'陕西',13,'laowang@qq.com','18565558897','深圳市南山区',8,9,29,'劳动合同','本科','信息管理与信息系统','深圳大学','2018-01-01','在职','00000051',1,'2018-04-01',NULL,'2018-01-01','2019-01-01',NULL),(52,'陈静6','女','1989-02-01','421288198902011234','已婚',1,'海南',1,'chenjing@qq.com','18795556693','海南省海口市美兰区',82,12,30,'劳动合同','高中','市场营销','武汉大学','2015-06-09','在职','00000052',3,'2015-09-10',NULL,'2015-06-09','2018-06-08',NULL),(53,'赵琳浩6','男','1993-03-04','610122199303041456','未婚',1,'陕西',3,'zhao@qq.com','15698887795','陕西省西安市莲湖区',91,12,33,'劳动合同','博士','电子工程','哈尔滨理工大学','2018-01-01','在职','00000053',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(54,'鹿存亮6','男','1990-01-03','610122199001031456','已婚',1,'陕西',3,'zhao@qq.com','15612347795','陕西省西安市莲湖区',92,12,34,'劳动合同','高中','电子工程','哈尔滨理工大学','2018-01-01','在职','00000054',3.5,'2018-04-01',NULL,'2018-01-01','2021-07-14',NULL),(55,'姚森6','男','1991-02-05','610122199102058952','已婚',1,'河南',3,'yaosen@qq.com','14785559936','河南洛阳人民大道58号',92,15,34,'劳动合同','初中','室内装修设计','西北大学','2017-01-02','在职','00000055',7,'2017-04-02',NULL,'2017-01-02','2024-01-17',NULL),(56,'云星6','女','1993-01-05','610122199301054789','已婚',1,'陕西西安',1,'yunxing@qq.com','15644442252','陕西西安新城区',92,16,34,'劳务合同','硕士','通信工程','西安电子科技学校','2018-01-01','在职','00000056',5.25,'2018-04-01',NULL,'2018-01-01','2023-04-13',NULL);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employeeec`
--

DROP TABLE IF EXISTS `employeeec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employeeec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL COMMENT '员工编号',
  `ecDate` date DEFAULT NULL COMMENT '奖罚日期',
  `ecReason` varchar(255) DEFAULT NULL COMMENT '奖罚原因',
  `ecPoint` int DEFAULT NULL COMMENT '奖罚分',
  `ecType` int DEFAULT NULL COMMENT '奖罚类别，0：奖，1：罚',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `pid` (`eid`),
  CONSTRAINT `employeeec_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeeec`
--

LOCK TABLES `employeeec` WRITE;
/*!40000 ALTER TABLE `employeeec` DISABLE KEYS */;
/*!40000 ALTER TABLE `employeeec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employeeremove`
--

DROP TABLE IF EXISTS `employeeremove`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employeeremove` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL,
  `afterDepId` int DEFAULT NULL COMMENT '调动后部门',
  `afterJobId` int DEFAULT NULL COMMENT '调动后职位',
  `removeDate` date DEFAULT NULL COMMENT '调动日期',
  `reason` varchar(255) DEFAULT NULL COMMENT '调动原因',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`eid`),
  CONSTRAINT `employeeremove_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeeremove`
--

LOCK TABLES `employeeremove` WRITE;
/*!40000 ALTER TABLE `employeeremove` DISABLE KEYS */;
/*!40000 ALTER TABLE `employeeremove` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employeetrain`
--

DROP TABLE IF EXISTS `employeetrain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employeetrain` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL COMMENT '员工编号',
  `trainDate` date DEFAULT NULL COMMENT '培训日期',
  `trainContent` varchar(255) DEFAULT NULL COMMENT '培训内容',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `pid` (`eid`),
  CONSTRAINT `employeetrain_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employeetrain`
--

LOCK TABLES `employeetrain` WRITE;
/*!40000 ALTER TABLE `employeetrain` DISABLE KEYS */;
/*!40000 ALTER TABLE `employeetrain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empsalary`
--

DROP TABLE IF EXISTS `empsalary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empsalary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eid` int DEFAULT NULL,
  `sid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eid` (`eid`),
  KEY `empsalary_ibfk_2` (`sid`),
  CONSTRAINT `empsalary_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`id`),
  CONSTRAINT `empsalary_ibfk_2` FOREIGN KEY (`sid`) REFERENCES `salary` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empsalary`
--

LOCK TABLES `empsalary` WRITE;
/*!40000 ALTER TABLE `empsalary` DISABLE KEYS */;
INSERT INTO `empsalary` (`id`, `eid`, `sid`) VALUES (6,4,10),(10,5,9),(11,6,13),(12,7,13),(14,8,10),(15,9,10),(20,10,13),(21,11,9),(22,3,13),(24,2,9),(25,1,13),(26,33,10),(28,34,9),(29,44,10),(30,45,10),(31,43,10),(32,47,10),(33,52,13),(34,53,10),(35,54,10),(36,56,10),(38,21,9);
/*!40000 ALTER TABLE `empsalary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`) VALUES (1,'1','vhr','SQL','V1__vhr.sql',-1039138481,'root','2022-02-24 07:43:40',228,1);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr`
--

DROP TABLE IF EXISTS `hr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hr` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'hrID',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `phone` char(11) DEFAULT NULL COMMENT '手机号码',
  `telephone` varchar(16) DEFAULT NULL COMMENT '住宅电话',
  `address` varchar(64) DEFAULT NULL COMMENT '联系地址',
  `enabled` tinyint(1) DEFAULT '1',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `userface` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr`
--

LOCK TABLES `hr` WRITE;
/*!40000 ALTER TABLE `hr` DISABLE KEYS */;
INSERT INTO `hr` (`id`, `name`, `phone`, `telephone`, `address`, `enabled`, `username`, `password`, `userface`, `remark`) VALUES (3,'系统管理员','18568887789','029-82881234','深圳南山',1,'admin','$2a$10$ySG2lkvjFHY5O0./CPIE1OI8VJsuKYEzOYzqIa7AJR6sEgSzUFOAm','http://localhost:8081/hr/img/add760c69f0e444c9e1d86253a8d7c36.jpg','super admin'),(5,'李白','18568123489','029-82123434','海口美兰',1,'libai','$2a$10$oE39aG10kB/rFu2vQeCJTu/V/v4n6DRR0f8WyXRiAYvBpmadoOBE.','http://localhost:8081/hr/img/558eaa905075471ea643cd48cd189b71.jpg','a poet'),(10,'韩愈','18568123666','029-82111555','广州番禺',1,'hanyu','$2a$10$oE39aG10kB/rFu2vQeCJTu/V/v4n6DRR0f8WyXRiAYvBpmadoOBE.','http://localhost:8081/hr/img/add760c69f0e444c9e1d86253a8d7c36.jpg','a poet.'),(11,'柳宗元','18568123377','029-82111333','广州天河',1,'liuzongyuan','$2a$10$oE39aG10kB/rFu2vQeCJTu/V/v4n6DRR0f8WyXRiAYvBpmadoOBE.','http://localhost:8081/hr/img/add760c69f0e444c9e1d86253a8d7c36.jpg','123'),(12,'曾巩','18568128888','029-82111222','广州越秀',1,'zenggong','$2a$10$oE39aG10kB/rFu2vQeCJTu/V/v4n6DRR0f8WyXRiAYvBpmadoOBE.','http://localhost:8081/hr/img/e9e4439d5d154a6c9109567d6bdaa2fa.jpg','ennnnnnn');
/*!40000 ALTER TABLE `hr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_role`
--

DROP TABLE IF EXISTS `hr_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hr_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hrid` int DEFAULT NULL,
  `rid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`),
  KEY `hr_role_ibfk_1` (`hrid`),
  CONSTRAINT `hr_role_ibfk_1` FOREIGN KEY (`hrid`) REFERENCES `hr` (`id`) ON DELETE CASCADE,
  CONSTRAINT `hr_role_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_role`
--

LOCK TABLES `hr_role` WRITE;
/*!40000 ALTER TABLE `hr_role` DISABLE KEYS */;
INSERT INTO `hr_role` (`id`, `hrid`, `rid`) VALUES (1,3,6),(35,12,4),(36,12,3),(37,12,2),(43,11,3),(44,11,2),(45,11,4),(46,11,5),(48,10,3),(49,10,4),(79,5,1),(80,5,2),(81,5,3),(82,5,4),(83,5,6);
/*!40000 ALTER TABLE `hr_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joblevel`
--

DROP TABLE IF EXISTS `joblevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `joblevel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '职称名称',
  `titleLevel` enum('正高级','副高级','中级','初级','员级') DEFAULT NULL,
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joblevel`
--

LOCK TABLES `joblevel` WRITE;
/*!40000 ALTER TABLE `joblevel` DISABLE KEYS */;
INSERT INTO `joblevel` (`id`, `name`, `titleLevel`, `createDate`, `enabled`) VALUES (9,'教授','正高级','2018-01-10 16:00:00',1),(10,'副教授','副高级','2018-01-11 13:19:20',1),(12,'助教','初级','2018-01-11 13:35:39',1),(13,'讲师','中级','2018-01-10 16:00:00',0),(14,'初级工程师','初级','2018-01-13 16:00:00',1),(15,'中级工程师66','中级','2018-01-13 16:00:00',1),(16,'高级工程师','副高级','2018-01-14 08:19:14',1),(17,'骨灰级工程师','正高级','2018-01-14 08:19:24',1);
/*!40000 ALTER TABLE `joblevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_send_log`
--

DROP TABLE IF EXISTS `mail_send_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mail_send_log` (
  `msgId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `empId` int DEFAULT NULL,
  `status` int DEFAULT '0' COMMENT '0发送中，1发送成功，2发送失败',
  `routeKey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `exchange` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `count` int DEFAULT '0' COMMENT '重试次数',
  `tryTime` datetime DEFAULT NULL COMMENT '第一次重试时间',
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_send_log`
--

LOCK TABLES `mail_send_log` WRITE;
/*!40000 ALTER TABLE `mail_send_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_send_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_3d`
--

DROP TABLE IF EXISTS `map_3d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `map_3d` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `type` int NOT NULL COMMENT '3d模型类型',
  `desp` varchar(255) DEFAULT NULL COMMENT '描述',
  `src` varchar(255) NOT NULL COMMENT '主要资源文件名',
  `mtl` varchar(255) DEFAULT NULL COMMENT '针对 obj+mtl 模型的mtl文件名',
  `dir_name` varchar(30) NOT NULL COMMENT '文件夹名称',
  `hr_id` int NOT NULL COMMENT '添加者',
  `insert_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '0 未删除，1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_3d`
--

LOCK TABLES `map_3d` WRITE;
/*!40000 ALTER TABLE `map_3d` DISABLE KEYS */;
INSERT INTO `map_3d` (`id`, `type`, `desp`, `src`, `mtl`, `dir_name`, `hr_id`, `insert_time`, `update_time`, `is_deleted`) VALUES (1,1,'json模型123123','scene.json','','json',3,'2022-03-05 21:42:47','2022-03-05 23:49:12',1),(2,2,'obj类型的模型：🌲','tree.obj','','obj',3,'2022-03-05 23:25:41','2022-03-05 23:25:41',0),(3,3,'该模型需要obj和mtl文件','male02.obj','male02.mtl','obj_mtl',3,'2022-03-05 23:29:33','2022-03-05 23:29:33',0),(4,4,'fbx模型。出不来','Mma_Kick.fbx','','fbx',3,'2022-03-05 23:30:08','2022-03-06 13:37:51',0),(5,5,'stl模型...','gear.stl','','stl',3,'2022-03-05 23:31:18','2022-03-05 23:31:18',0),(6,6,'dae模型。角度问题','elf.dae','','dae',3,'2022-03-05 23:31:42','2022-03-06 13:25:21',0),(7,8,'小黄鸭','Duck.gltf','','gltf',3,'2022-03-05 23:32:11','2022-03-05 23:32:11',0),(8,7,'ply模型。需要调整角度','Lucy100k.ply','','ply',3,'2022-03-05 23:32:56','2022-03-06 13:24:24',0),(9,1,'json模型✨✨✨','scene.json','','json',3,'2022-03-05 23:50:36','2022-03-05 23:50:36',0),(10,8,'油气站模型','model.gltf','','gltf_petro_site',3,'2022-03-06 12:58:45','2022-03-06 12:58:45',0),(11,1,'另一个json模型','scene.json','','json2',3,'2022-03-06 13:58:48','2022-03-06 13:58:48',0),(12,5,'slotted_disk','slotted_disk.stl','','stl2',3,'2022-03-06 13:59:36','2022-03-06 13:59:36',0),(13,2,'LeePerrySmith','LeePerrySmith.obj','','obj2',3,'2022-03-06 13:59:52','2022-03-06 14:03:27',0),(14,8,'VC','VC.gltf','','gltf2',3,'2022-03-06 14:00:16','2022-03-06 14:00:16',1),(15,6,'dae33','dae3.dae','','dae3',3,'2022-03-06 21:48:42','2022-03-06 21:48:42',1),(16,3,'obj_mtl，模型加载不全','obj3.obj','obj3.mtl','obj3',3,'2022-03-06 21:50:26','2022-03-07 11:46:57',0),(17,2,'obj4','obj4.obj','','obj4',3,'2022-03-06 21:52:53','2022-03-06 21:52:53',0);
/*!40000 ALTER TABLE `map_3d` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `map_poi`
--

DROP TABLE IF EXISTS `map_poi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `map_poi` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `remark` varchar(1000) NOT NULL DEFAULT '' COMMENT '地图备注',
  `hr_id` int NOT NULL COMMENT '地图添加者',
  `zoom` int NOT NULL DEFAULT '17' COMMENT '地图缩放等级',
  `lat` char(30) NOT NULL DEFAULT '116.2550699295006' COMMENT '中心点的纬度，默认CUP',
  `lng` char(30) NOT NULL DEFAULT '40.225234327805275' COMMENT '中心点的经度，默认CUP',
  `data` text NOT NULL COMMENT '地图覆盖层的JSON数据，包括标注点及其标注的信息等',
  `insert_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '做逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map_poi`
--

LOCK TABLES `map_poi` WRITE;
/*!40000 ALTER TABLE `map_poi` DISABLE KEYS */;
INSERT INTO `map_poi` (`id`, `remark`, `hr_id`, `zoom`, `lat`, `lng`, `data`, `insert_time`, `update_time`, `is_deleted`) VALUES (1,'一张地图～一张地图～一张地图～一张地图～一张地图',3,5,'33.44410403359708','123.89646981567329','[{\"remark\":\"\",\"address\":\"\",\"pt\":{\"lng\":123.21422472916522,\"lat\":24.28122083849392}},{\"remark\":\"陕西省咸阳市秦都区马庄街道\",\"address\":\"陕西省咸阳市秦都区马庄街道\",\"pt\":{\"lng\":108.64356564623095,\"lat\":34.44499875094979}},{\"remark\":\"陕西省咸阳市秦都区双照街道\",\"address\":\"陕西省咸阳市秦都区双照街道\",\"pt\":{\"lng\":108.69407736505762,\"lat\":34.37968973867366}},{\"remark\":\"安徽省安庆市岳西县店前镇\",\"address\":\"安徽省安庆市岳西县店前镇\",\"pt\":{\"lng\":116.01390184700277,\"lat\":30.711939270924333}}]','2022-03-03 02:42:05','2022-03-06 21:59:56',0),(2,'中国石油大学(BJ)',3,17,'40.223920558557886','116.25750882895866','[{\"remark\":\"北京市北京市昌平区城北街道\",\"address\":\"北京市北京市昌平区城北街道\",\"pt\":{\"lng\":116.25397399677865,\"lat\":40.22559947393615}},{\"remark\":\"北京市北京市昌平区城北街道亢山路辅路aasda\",\"address\":\"北京市北京市昌平区城北街道亢山路辅路\",\"pt\":{\"lng\":116.25653416748209,\"lat\":40.223967501206516}},{\"remark\":\"北京市北京市昌平区城北街道\",\"address\":\"北京市北京市昌平区城北街道\",\"pt\":{\"lng\":116.25391111539295,\"lat\":40.22428425850651}},{\"remark\":\"北京市北京市昌平区城北街道中石路\",\"address\":\"北京市北京市昌平区城北街道中石路\",\"pt\":{\"lng\":116.25262653851368,\"lat\":40.22210135724414}},{\"remark\":\"北京市北京市昌平区城北街道中石路\",\"address\":\"北京市北京市昌平区城北街道中石路\",\"pt\":{\"lng\":116.2543917088408,\"lat\":40.222535188691786}},{\"remark\":\"北京市北京市昌平区城北街道府学路甲20号\",\"address\":\"北京市北京市昌平区城北街道府学路甲20号\",\"pt\":{\"lng\":116.2520695890975,\"lat\":40.22651873226635}}]','2022-03-03 03:50:11','2022-03-06 14:09:33',0),(3,'blablabla...',3,11,'39.97998018132824','116.25450949365943','[]','2022-03-03 03:50:47','2022-03-05 19:29:35',0),(4,'test',3,14,'39.92444949516435','116.42643067394008','[{\"remark\":\"北京市北京市西城区西长安街街道文津街七号\",\"address\":\"北京市北京市西城区西长安街街道文津街七号\",\"pt\":{\"lng\":116.39191778865573,\"lat\":39.92842839644556}},{\"remark\":\"北京市北京市东城区东华门街道东厂胡同3号院\",\"address\":\"北京市北京市东城区东华门街道东厂胡同3号院\",\"pt\":{\"lng\":116.41419576530318,\"lat\":39.929313668475814}},{\"remark\":\"北京市北京市西城区西长安街街道\",\"address\":\"北京市北京市西城区西长安街街道\",\"pt\":{\"lng\":116.39256456862292,\"lat\":39.923725195290196}},{\"remark\":\"北京市北京市东城区东华门街道韶九胡同15号院-3号楼\",\"address\":\"北京市北京市东城区东华门街道韶九胡同15号院-3号楼\",\"pt\":{\"lng\":116.41563305411914,\"lat\":39.92383586258818}},{\"remark\":\"北京市北京市西城区西长安街街道\",\"address\":\"北京市北京市西城区西长安街街道\",\"pt\":{\"lng\":116.3930676197085,\"lat\":39.920349756087944}},{\"remark\":\"北京市北京市东城区东华门街道王府井大街253号\",\"address\":\"北京市北京市东城区东华门街道王府井大街253号\",\"pt\":{\"lng\":116.41642356296792,\"lat\":39.92062643774302}},{\"remark\":\"北京市北京市西城区西长安街街道\",\"address\":\"北京市北京市西城区西长安街街道\",\"pt\":{\"lng\":116.3934269419125,\"lat\":39.91642075497279}},{\"remark\":\"北京市北京市东城区东华门街道大纱帽胡同1号\",\"address\":\"北京市北京市东城区东华门街道大纱帽胡同1号\",\"pt\":{\"lng\":116.41635169852712,\"lat\":39.91758287755856}},{\"remark\":\"北京市北京市西城区西长安街街道西长安街10号\",\"address\":\"北京市北京市西城区西长安街街道西长安街10号\",\"pt\":{\"lng\":116.39400185743888,\"lat\":39.91326632203961}},{\"remark\":\"北京市北京市东城区东华门街道东长安街6号-4楼-405室\",\"address\":\"北京市北京市东城区东华门街道东长安街6号-4楼-405室\",\"pt\":{\"lng\":116.4169984784943,\"lat\":39.914151791682364}},{\"remark\":\"北京市北京市西城区西长安街街道后红井胡同\",\"address\":\"北京市北京市西城区西长安街街道后红井胡同\",\"pt\":{\"lng\":116.39630151954442,\"lat\":39.90972432808049}},{\"remark\":\"北京市北京市东城区东华门街道台基厂三条5号\",\"address\":\"北京市北京市东城区东华门街道台基厂三条5号\",\"pt\":{\"lng\":116.41757339402069,\"lat\":39.910167087421776}},{\"remark\":\"北京市北京市西城区西长安街街道西交民巷23号\",\"address\":\"北京市北京市西城区西长安街街道西交民巷23号\",\"pt\":{\"lng\":116.39888863941316,\"lat\":39.90850672501798}},{\"remark\":\"北京市北京市东城区东华门街道西交民巷14号\",\"address\":\"北京市北京市东城区东华门街道西交民巷14号\",\"pt\":{\"lng\":116.40320050586105,\"lat\":39.90806395485925}},{\"remark\":\"北京市北京市东城区东华门街道东交民巷42号\",\"address\":\"北京市北京市东城区东华门街道东交民巷42号\",\"pt\":{\"lng\":116.40722491454575,\"lat\":39.90795326186885}},{\"remark\":\"北京市北京市东城区东华门街道前门东大街9号\",\"address\":\"北京市北京市东城区东华门街道前门东大街9号\",\"pt\":{\"lng\":116.41096186546726,\"lat\":39.90795326186885}},{\"remark\":\"北京市北京市东城区东华门街道东交民巷28号\",\"address\":\"北京市北京市东城区东华门街道东交民巷28号\",\"pt\":{\"lng\":116.41412390086238,\"lat\":39.90850672501798}},{\"remark\":\"北京市北京市东城区东华门街道东交民巷19号\",\"address\":\"北京市北京市东城区东华门街道东交民巷19号\",\"pt\":{\"lng\":116.41584864744154,\"lat\":39.90894949229205}},{\"remark\":\"北京市北京市西城区什刹海街道\",\"address\":\"北京市北京市西城区什刹海街道\",\"pt\":{\"lng\":116.39529541737325,\"lat\":39.93152679806406}},{\"remark\":\"北京市北京市西城区什刹海街道景山西街38号\",\"address\":\"北京市北京市西城区什刹海街道景山西街38号\",\"pt\":{\"lng\":116.40061338599232,\"lat\":39.9323013763803}},{\"remark\":\"北京市北京市西城区什刹海街道\",\"address\":\"北京市北京市西城区什刹海街道\",\"pt\":{\"lng\":116.40406287915063,\"lat\":39.93241202970422}},{\"remark\":\"北京市北京市东城区景山街道大学夹道12号\",\"address\":\"北京市北京市东城区景山街道大学夹道12号\",\"pt\":{\"lng\":116.40779983007214,\"lat\":39.932522682847846}},{\"remark\":\"北京市北京市东城区景山街道沙滩后街5号\",\"address\":\"北京市北京市东城区景山街道沙滩后街5号\",\"pt\":{\"lng\":116.40981203441449,\"lat\":39.931858761281475}},{\"remark\":\"北京市北京市东城区景山街道五四大街29-1\",\"address\":\"北京市北京市东城区景山街道五四大街29-1\",\"pt\":{\"lng\":116.41124932323045,\"lat\":39.931194833223806}},{\"remark\":\"北京市北京市东城区东华门街道富强胡同26号\",\"address\":\"北京市北京市东城区东华门街道富强胡同26号\",\"pt\":{\"lng\":116.41513000303355,\"lat\":39.927875095566534}}]','2022-03-03 03:52:08','2022-03-05 19:01:57',0),(5,'testttt',3,5,'35.20954825318285','115.6713915273867','[{\"remark\":\"河南省漯河市郾城区新店镇\",\"address\":\"河南省漯河市郾城区新店镇\",\"pt\":{\"lng\":113.8103853824261,\"lat\":33.64089013997609}},{\"remark\":\"河南省商丘市梁园区双八镇\",\"address\":\"河南省商丘市梁园区双八镇\",\"pt\":{\"lng\":115.71654475708988,\"lat\":34.49087717900949}},{\"remark\":\"山东省济南市章丘区曹范街道\",\"address\":\"山东省济南市章丘区曹范街道\",\"pt\":{\"lng\":117.39121978712137,\"lat\":36.596092045460935}},{\"remark\":\"河北省石家庄市灵寿县灵寿镇\",\"address\":\"河北省石家庄市灵寿县灵寿镇\",\"pt\":{\"lng\":114.37406310464822,\"lat\":38.32651484872594}},{\"remark\":\"山西省太原市阳曲县高村乡\",\"address\":\"山西省太原市阳曲县高村乡\",\"pt\":{\"lng\":112.60792260759077,\"lat\":38.21047724310834}},{\"remark\":\"陕西省西安市临潼区代王街道\",\"address\":\"陕西省西安市临潼区代王街道\",\"pt\":{\"lng\":109.33320376929673,\"lat\":34.399711939620616}},{\"remark\":\"安徽省合肥市肥东县桥头集镇\",\"address\":\"安徽省合肥市肥东县桥头集镇\",\"pt\":{\"lng\":117.53839816187616,\"lat\":31.704217847237253}},{\"remark\":\"江苏省镇江市丹徒区上党镇\",\"address\":\"江苏省镇江市丹徒区上党镇\",\"pt\":{\"lng\":119.4149224399997,\"lat\":31.986904935577492}},{\"remark\":\"上海市上海市松江区新桥镇新育路\",\"address\":\"上海市上海市松江区新桥镇新育路\",\"pt\":{\"lng\":121.32824131181195,\"lat\":31.07290612353555}},{\"remark\":\"浙江省杭州市钱塘区临江街道\",\"address\":\"浙江省杭州市钱塘区临江街道\",\"pt\":{\"lng\":120.66593862541541,\"lat\":30.24583674098498}},{\"remark\":\"江西省宜春市靖安县雷公尖乡\",\"address\":\"江西省宜春市靖安县雷公尖乡\",\"pt\":{\"lng\":115.40431171272185,\"lat\":28.862483587712997}},{\"remark\":\"湖南省株洲市石峰区清水塘街道\",\"address\":\"湖南省株洲市石峰区清水塘街道\",\"pt\":{\"lng\":113.10597910009231,\"lat\":27.91684710451047}},{\"remark\":\"福建省福州市福清市东张镇\",\"address\":\"福建省福州市福清市东张镇\",\"pt\":{\"lng\":119.2397528490772,\"lat\":25.75696477765322}},{\"remark\":\"广东省广州市花都区赤坭镇\",\"address\":\"广东省广州市花都区赤坭镇\",\"pt\":{\"lng\":113.05826111141805,\"lat\":23.401768033551203}},{\"remark\":\"广西壮族自治区崇左市扶绥县中东镇\",\"address\":\"广西壮族自治区崇左市扶绥县中东镇\",\"pt\":{\"lng\":107.87022340302555,\"lat\":22.8231525540423}},{\"remark\":\"贵州省贵阳市清镇市卫城镇\",\"address\":\"贵州省贵阳市清镇市卫城镇\",\"pt\":{\"lng\":106.28805587493422,\"lat\":26.752334639623378}},{\"remark\":\"重庆市重庆市巴南区圣灯山镇\",\"address\":\"重庆市重庆市巴南区圣灯山镇\",\"pt\":{\"lng\":106.65600181169964,\"lat\":29.298894370662595}},{\"remark\":\"四川省成都市双流区永兴街道\",\"address\":\"四川省成都市双流区永兴街道\",\"pt\":{\"lng\":104.1171748594996,\"lat\":30.35763336355922}},{\"remark\":\"云南省昆明市安宁市草铺街道上平公路\",\"address\":\"云南省昆明市安宁市草铺街道上平公路\",\"pt\":{\"lng\":102.42462353889725,\"lat\":24.920981591303484}},{\"remark\":\"青海省海东市互助土族自治县丹麻镇\",\"address\":\"青海省海东市互助土族自治县丹麻镇\",\"pt\":{\"lng\":102.09347220662126,\"lat\":36.81833913967522}},{\"remark\":\"甘肃省兰州市皋兰县石洞镇\",\"address\":\"甘肃省兰州市皋兰县石洞镇\",\"pt\":{\"lng\":103.93320189105611,\"lat\":36.40303189215962}},{\"remark\":\"宁夏回族自治区银川市西夏区镇北堡镇\",\"address\":\"宁夏回族自治区银川市西夏区镇北堡镇\",\"pt\":{\"lng\":105.95690454393444,\"lat\":38.54362169366385}},{\"remark\":\"辽宁省沈阳市沈北新区辉山街道\",\"address\":\"辽宁省沈阳市沈北新区辉山街道\",\"pt\":{\"lng\":123.5815149208203,\"lat\":41.926489442049956}},{\"remark\":\"吉林省长春市双阳区鹿乡镇\",\"address\":\"吉林省长春市双阳区鹿乡镇\",\"pt\":{\"lng\":125.53162838632123,\"lat\":43.6072047834114}},{\"remark\":\"内蒙古自治区呼和浩特市武川县哈乐镇\",\"address\":\"内蒙古自治区呼和浩特市武川县哈乐镇\",\"pt\":{\"lng\":111.66006656568247,\"lat\":41.09677243721866}},{\"remark\":\"北京市北京市顺义区李遂镇\",\"address\":\"北京市北京市顺义区李遂镇\",\"pt\":{\"lng\":116.81130968210006,\"lat\":40.08699529731208}},{\"remark\":\"天津市天津市东丽区东丽湖街道\",\"address\":\"天津市天津市东丽区东丽湖街道\",\"pt\":{\"lng\":117.5104069621853,\"lat\":39.176545902054386}},{\"remark\":\"广东省江门市新会区睦洲镇\",\"address\":\"广东省江门市新会区睦洲镇\",\"pt\":{\"lng\":113.16864489244769,\"lat\":22.447420055260395}},{\"remark\":\"广东省深圳市龙岗区龙岗街道黄屋路1号\",\"address\":\"广东省深圳市龙岗区龙岗街道黄屋路1号\",\"pt\":{\"lng\":114.3092772964205,\"lat\":22.720783225487164}},{\"remark\":\"台湾省新北市乌来区乌来区\",\"address\":\"台湾省新北市乌来区乌来区\",\"pt\":{\"lng\":121.52101765702285,\"lat\":24.719481733827774}}]','2022-03-03 03:53:28','2022-03-05 19:02:12',0),(6,'tesssst',3,1333,'36','110','[]','2022-03-03 03:54:00','2022-03-03 03:54:00',1),(7,'呜呜呜呜呜',3,4,'36.435704308652014','104.00016441421764','[]','2022-03-03 04:33:12','2022-03-03 14:37:40',0),(8,'mmmmmm',3,17,'40.22666667972294','116.24455975503373','[]','2022-03-03 10:22:31','2022-03-03 11:57:57',0),(9,'默认地址是中国石油大学(BJ)～',3,17,'40.22526196837495','116.25541128559426','[]','2022-03-05 19:16:37','2022-03-05 19:35:16',0);
/*!40000 ALTER TABLE `map_poi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(64) DEFAULT NULL,
  `path` varchar(64) DEFAULT NULL,
  `component` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `iconCls` varchar(64) DEFAULT NULL,
  `keepAlive` tinyint(1) DEFAULT NULL,
  `requireAuth` tinyint(1) DEFAULT NULL,
  `parentId` int DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parentId` (`parentId`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parentId`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `url`, `path`, `component`, `name`, `iconCls`, `keepAlive`, `requireAuth`, `parentId`, `enabled`) VALUES (1,'/',NULL,NULL,'所有',NULL,NULL,NULL,NULL,1),(2,'/','/home','Home','员工资料','fa fa-user-circle-o',NULL,1,1,1),(3,'/','/home','Home','人事管理','fa fa-address-card-o',NULL,1,1,1),(4,'/','/home','Home','薪资管理','fa fa-money',NULL,1,1,1),(5,'/','/home','Home','统计管理','fa fa-bar-chart',NULL,1,1,1),(6,'/','/home','Home','系统管理','fa fa-windows',NULL,1,1,1),(7,'/employee/basic/**','/emp/basic','EmpBasic','基本资料',NULL,NULL,1,2,1),(8,'/employee/advanced/**','/emp/adv','EmpAdv','高级资料',NULL,NULL,1,2,1),(9,'/personnel/emp/**','/per/emp','PerEmp','员工资料',NULL,NULL,1,3,1),(10,'/personnel/ec/**','/per/ec','PerEc','员工奖惩',NULL,NULL,1,3,1),(11,'/personnel/train/**','/per/train','PerTrain','员工培训',NULL,NULL,1,3,1),(12,'/personnel/salary/**','/per/salary','PerSalary','员工调薪',NULL,NULL,1,3,1),(13,'/personnel/remove/**','/per/mv','PerMv','员工调动',NULL,NULL,1,3,1),(14,'/salary/sob/**','/sal/sob','SalSob','工资账套管理',NULL,NULL,1,4,1),(15,'/salary/sobcfg/**','/sal/sobcfg','SalSobCfg','员工账套设置',NULL,NULL,1,4,1),(16,'/salary/table/**','/sal/table','SalTable','工资表管理',NULL,NULL,1,4,1),(17,'/salary/month/**','/sal/month','SalMonth','月末处理',NULL,NULL,1,4,1),(18,'/salary/search/**','/sal/search','SalSearch','工资表查询',NULL,NULL,1,4,1),(19,'/statistics/all/**','/sta/all','StaAll','综合信息统计',NULL,NULL,1,5,1),(20,'/statistics/score/**','/sta/score','StaScore','员工积分统计',NULL,NULL,1,5,1),(21,'/statistics/personnel/**','/sta/pers','StaPers','人事信息统计',NULL,NULL,1,5,1),(22,'/statistics/recored/**','/sta/record','StaRecord','人事记录统计',NULL,NULL,1,5,1),(23,'/system/basic/**','/sys/basic','SysBasic','基础信息设置',NULL,NULL,1,6,1),(24,'/system/cfg/**','/sys/cfg','SysCfg','系统管理',NULL,NULL,1,6,1),(25,'/system/log/**','/sys/log','SysLog','操作日志管理',NULL,NULL,1,6,1),(26,'/system/hr/**','/sys/hr','SysHr','操作员管理',NULL,NULL,1,6,1),(27,'/system/data/**','/sys/data','SysData','备份恢复数据库',NULL,NULL,1,6,1),(28,'/system/init/**','/sys/init','SysInit','初始化数据库',NULL,NULL,1,6,1),(30,'/','/home','Home','学习管理','fa fa-book',NULL,1,1,1),(31,'/study/lesson/**','/study/lesson','StudyLesson','课程管理',NULL,NULL,1,30,1),(32,'/study/doc/**','/study/doc','StudyDoc','文档管理',NULL,NULL,1,30,1),(33,'/study/progress/**','/study/progress','StudyProgress','进度管理',NULL,NULL,1,30,1),(34,'/','/home','Home','地图管理','fa fa-map-marker',NULL,1,1,1),(35,'/map/poi/**','/map/poi','MapPOI','POI管理',NULL,NULL,1,34,1),(36,'/map/3d/**','/map/3d','Map3D','3D模型管理',NULL,NULL,1,34,1),(37,'/','/three','Home','三维可视化管理','fa fa-globe',NULL,1,1,1),(38,'/three/cesium/**','/three/cesium','ThreeCesium','Cesium联动',NULL,NULL,1,37,1),(39,'/three/site/**','/three/site','ThreePetroSite','汽油站查看',NULL,NULL,1,37,1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_role`
--

DROP TABLE IF EXISTS `menu_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mid` int DEFAULT NULL,
  `rid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `rid` (`rid`),
  CONSTRAINT `menu_role_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `menu` (`id`),
  CONSTRAINT `menu_role_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_role`
--

LOCK TABLES `menu_role` WRITE;
/*!40000 ALTER TABLE `menu_role` DISABLE KEYS */;
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (161,7,3),(162,7,6),(163,9,6),(164,10,6),(165,11,6),(166,12,6),(167,13,6),(168,14,6),(169,15,6),(170,16,6),(171,17,6),(172,18,6),(173,19,6),(174,20,6),(175,21,6),(176,22,6),(177,23,6),(178,25,6),(179,26,6),(180,27,6),(181,28,6),(182,24,6),(247,7,4),(248,8,4),(249,11,4),(250,7,2),(251,8,2),(252,9,2),(253,10,2),(254,12,2),(255,13,2),(256,7,1),(257,8,1),(258,9,1),(259,10,1),(260,11,1),(261,12,1),(262,13,1),(263,14,1),(264,15,1),(265,16,1),(266,17,1),(267,18,1),(268,19,1),(269,20,1),(270,21,1),(271,22,1),(272,23,1),(273,24,1),(274,25,1),(275,26,1),(276,27,1),(277,28,1),(280,7,14),(281,8,14),(282,9,14),(283,31,6),(284,32,6),(285,33,6),(286,35,6),(287,36,6),(288,38,6),(289,39,6);
/*!40000 ALTER TABLE `menu_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msgcontent`
--

DROP TABLE IF EXISTS `msgcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `msgcontent` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msgcontent`
--

LOCK TABLES `msgcontent` WRITE;
/*!40000 ALTER TABLE `msgcontent` DISABLE KEYS */;
INSERT INTO `msgcontent` (`id`, `title`, `message`, `createDate`) VALUES (14,'2222222222','11111111111111111','2018-02-02 12:46:19'),(15,'22222222','3333333333333333333333','2018-02-02 13:45:57'),(16,'通知标题1','通知内容1','2018-02-03 03:41:39'),(17,'通知标题2','通知内容2','2018-02-03 03:52:37'),(18,'通知标题3','通知内容3','2018-02-03 04:19:41');
/*!40000 ALTER TABLE `msgcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nation`
--

DROP TABLE IF EXISTS `nation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nation`
--

LOCK TABLES `nation` WRITE;
/*!40000 ALTER TABLE `nation` DISABLE KEYS */;
INSERT INTO `nation` (`id`, `name`) VALUES (1,'汉族'),(2,'蒙古族'),(3,'回族'),(4,'藏族'),(5,'维吾尔族'),(6,'苗族'),(7,'彝族'),(8,'壮族'),(9,'布依族'),(10,'朝鲜族'),(11,'满族'),(12,'侗族'),(13,'瑶族'),(14,'白族'),(15,'土家族'),(16,'哈尼族'),(17,'哈萨克族'),(18,'傣族'),(19,'黎族'),(20,'傈僳族'),(21,'佤族'),(22,'畲族'),(23,'高山族'),(24,'拉祜族'),(25,'水族'),(26,'东乡族'),(27,'纳西族'),(28,'景颇族'),(29,'柯尔克孜族'),(30,'土族'),(31,'达斡尔族'),(32,'仫佬族'),(33,'羌族'),(34,'布朗族'),(35,'撒拉族'),(36,'毛难族'),(37,'仡佬族'),(38,'锡伯族'),(39,'阿昌族'),(40,'普米族'),(41,'塔吉克族'),(42,'怒族'),(43,'乌孜别克族'),(44,'俄罗斯族'),(45,'鄂温克族'),(46,'崩龙族'),(47,'保安族'),(48,'裕固族'),(49,'京族'),(50,'塔塔尔族'),(51,'独龙族'),(52,'鄂伦春族'),(53,'赫哲族'),(54,'门巴族'),(55,'珞巴族'),(56,'基诺族');
/*!40000 ALTER TABLE `nation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oplog`
--

DROP TABLE IF EXISTS `oplog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oplog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `addDate` date DEFAULT NULL COMMENT '添加日期',
  `operate` varchar(255) DEFAULT NULL COMMENT '操作内容',
  `hrid` int DEFAULT NULL COMMENT '操作员ID',
  PRIMARY KEY (`id`),
  KEY `hrid` (`hrid`),
  CONSTRAINT `oplog_ibfk_1` FOREIGN KEY (`hrid`) REFERENCES `hr` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oplog`
--

LOCK TABLES `oplog` WRITE;
/*!40000 ALTER TABLE `oplog` DISABLE KEYS */;
/*!40000 ALTER TABLE `oplog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `politicsstatus`
--

DROP TABLE IF EXISTS `politicsstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `politicsstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `politicsstatus`
--

LOCK TABLES `politicsstatus` WRITE;
/*!40000 ALTER TABLE `politicsstatus` DISABLE KEYS */;
INSERT INTO `politicsstatus` (`id`, `name`) VALUES (1,'中共党员'),(2,'中共预备党员'),(3,'共青团员'),(4,'民革党员'),(5,'民盟盟员'),(6,'民建会员'),(7,'民进会员'),(8,'农工党党员'),(9,'致公党党员'),(10,'九三学社社员'),(11,'台盟盟员'),(12,'无党派民主人士'),(13,'普通公民');
/*!40000 ALTER TABLE `politicsstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `position` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '职位',
  `createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` (`id`, `name`, `createDate`, `enabled`) VALUES (29,'技术总监','2018-01-11 13:13:42',1),(30,'运营总监','2018-01-11 13:13:48',1),(31,'市场总监','2018-01-10 16:00:00',1),(33,'研发工程师','2018-01-13 16:00:00',0),(34,'运维工程师','2018-01-14 08:11:41',1),(36,'Java研发经理','2019-09-30 16:00:00',1);
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `nameZh` varchar(64) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `nameZh`) VALUES (1,'ROLE_manager','部门经理'),(2,'ROLE_personnel','人事专员'),(3,'ROLE_recruiter','招聘主管'),(4,'ROLE_train','培训主管'),(5,'ROLE_performance','薪酬绩效主管'),(6,'ROLE_admin','系统管理员'),(13,'ROLE_test2','测试角色2'),(14,'ROLE_test1','测试角色1'),(17,'ROLE_test3','测试角色3'),(18,'ROLE_test4','测试角色4'),(19,'ROLE_test4','测试角色4'),(20,'ROLE_test5','测试角色5'),(21,'ROLE_test6','测试角色6');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary`
--

DROP TABLE IF EXISTS `salary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `basicSalary` int DEFAULT NULL COMMENT '基本工资',
  `bonus` int DEFAULT NULL COMMENT '奖金',
  `lunchSalary` int DEFAULT NULL COMMENT '午餐补助',
  `trafficSalary` int DEFAULT NULL COMMENT '交通补助',
  `allSalary` int DEFAULT NULL COMMENT '应发工资',
  `pensionBase` int DEFAULT NULL COMMENT '养老金基数',
  `pensionPer` float DEFAULT NULL COMMENT '养老金比率',
  `createDate` timestamp NULL DEFAULT NULL COMMENT '启用时间',
  `medicalBase` int DEFAULT NULL COMMENT '医疗基数',
  `medicalPer` float DEFAULT NULL COMMENT '医疗保险比率',
  `accumulationFundBase` int DEFAULT NULL COMMENT '公积金基数',
  `accumulationFundPer` float DEFAULT NULL COMMENT '公积金比率',
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary`
--

LOCK TABLES `salary` WRITE;
/*!40000 ALTER TABLE `salary` DISABLE KEYS */;
INSERT INTO `salary` (`id`, `basicSalary`, `bonus`, `lunchSalary`, `trafficSalary`, `allSalary`, `pensionBase`, `pensionPer`, `createDate`, `medicalBase`, `medicalPer`, `accumulationFundBase`, `accumulationFundPer`, `name`) VALUES (9,9000,4000,800,500,NULL,2000,0.07,'2018-01-23 16:00:00',2000,0.07,2000,0.07,'市场部工资账套'),(10,2000,2000,400,1000,NULL,2000,0.07,'2017-12-31 16:00:00',2000,0.07,2000,0.07,'人事部工资账套'),(13,20000,3000,500,500,NULL,4000,0.07,'2018-01-24 16:00:00',4000,0.07,4000,0.07,'运维部工资账套');
/*!40000 ALTER TABLE `salary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_doc`
--

DROP TABLE IF EXISTS `study_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `study_doc` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `fname` varchar(255) DEFAULT NULL COMMENT '文件名',
  `desp` text COMMENT '描述',
  `link` varchar(255) DEFAULT NULL COMMENT '服务端文件标识',
  `hrid` int NOT NULL COMMENT '上传者',
  `is_deleted` tinyint DEFAULT '0' COMMENT '0 未删除，1 已删除',
  `insert_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_doc`
--

LOCK TABLES `study_doc` WRITE;
/*!40000 ALTER TABLE `study_doc` DISABLE KEYS */;
INSERT INTO `study_doc` (`id`, `fname`, `desp`, `link`, `hrid`, `is_deleted`, `insert_time`, `update_time`) VALUES (7,'grass.jpeg','grass pic','http://localhost:8081/study/doc/b138d324916948e292f490792b9b261d.jpg',3,0,'2022-02-28 15:06:04','2022-02-28 15:06:04'),(8,'92cdce3c57e95fb084e7cb4da8b8b249.jpeg','卡通小银儿','http://localhost:8081/study/doc/cc9b52b74b6d4897a0a43fab88c3a131.jpg',3,1,'2022-02-28 15:08:15','2022-02-28 15:08:15'),(9,'R-C.jpeg','my avatar','http://localhost:8081/study/doc/c758047123f54e52b0663b7a9bafe898.jpg',3,0,'2022-02-28 15:08:32','2022-02-28 15:08:32'),(10,'th.jpeg','仓鼠🐹','http://localhost:8081/study/doc/be6ae4f08b5b4096ab4e47566e26c695.jpg',3,0,'2022-02-28 15:08:45','2022-02-28 15:08:45'),(11,'本科生毕业设计开题报告模板.docx','开题报告模版','http://localhost:8081/study/doc/c64957114ce342d0a9fba9a88c1a7aa8.docx',3,0,'2022-02-28 15:09:16','2022-02-28 15:09:16'),(12,'Spring Cloud 训练营3天笔记(1).pdf','SpringCloud笔记','http://localhost:8081/study/doc/77ff018e3c9d4c168b0e5eec1d5eae31.pdf',3,0,'2022-02-28 15:09:51','2022-02-28 15:09:51'),(13,'演讲.pptx','开题报告演讲PPT','http://localhost:8081/study/doc/c4528b01497d483ea3247e9099fc6d60.jar',3,0,'2022-02-28 15:10:36','2022-02-28 15:10:36'),(14,'答辩1.pptx','答辩PPT模版','http://localhost:8081/study/doc/3b1d7703202a452aa0b476beaa2d2c37.pptx',3,0,'2022-02-28 15:12:47','2022-02-28 15:12:47');
/*!40000 ALTER TABLE `study_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_lesson`
--

DROP TABLE IF EXISTS `study_lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `study_lesson` (
  `id` int NOT NULL AUTO_INCREMENT,
  `desp` varchar(255) DEFAULT NULL COMMENT '课程描述',
  `total` int DEFAULT '0' COMMENT '课程视频数量',
  `finished` int DEFAULT '0' COMMENT '完成的视频数量',
  `link` varchar(255) DEFAULT NULL COMMENT '资源链接',
  `hrid` int NOT NULL COMMENT '添加者',
  `insert_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '最新修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_lesson`
--

LOCK TABLES `study_lesson` WRITE;
/*!40000 ALTER TABLE `study_lesson` DISABLE KEYS */;
INSERT INTO `study_lesson` (`id`, `desp`, `total`, `finished`, `link`, `hrid`, `insert_time`, `update_time`) VALUES (2,'【尚硅谷】全新2022版Docker与微服务实战教程（从入门到进阶）',955,110,'https://www.bilibili.com/video/BV1gr4y1U7CY?from=search&seid=13229240429326525210&spm_id_from=333.337.0.0',3,'2022-02-27 14:02:47','2022-02-28 00:20:55'),(13,'全新2022版Docker与微服务实战教程',95,1,'https://www.bilibili.com/video/BV1gr4y1U7CY',3,'2022-02-27 14:02:47','2022-02-28 20:30:41'),(15,'黑马程序员Netty全套教程，全网最全Netty深入浅出教程，Java网络编程的王者',157,0,'https://www.bilibili.com/video/BV1py4y1E7oA?p=2&spm_id_from=pageDriver',3,'2022-02-27 23:35:30','2022-02-28 00:46:51'),(16,'Docker与微服务从入门到实战',95,0,'https://www.bilibili.com/video/BV1gr4y1U7CY?from=search&seid=13229240429326525210&spm_id_from=333.337.0.0',3,'2022-02-27 23:37:05','2022-02-27 23:37:05'),(17,'Java教程',99,0,'https://www.baidu.com',3,'2022-02-28 00:32:20','2022-02-28 00:46:34'),(18,'123aaa',99,0,'bbbb',3,'2022-02-28 00:32:29','2022-02-28 00:32:29'),(19,'SpringCloud全套教程',99,0,'https://www.bilibili.com/video/BV18E411x7eT?from=search&seid=13229240429326525210&spm_id_from=333.337.0.0',3,'2022-02-28 00:45:31','2022-02-28 00:45:31'),(20,'【MIT 6.824 Distributed Systems Spring 2020 分布式系统 中文翻译版合集】',31,0,'https://www.bilibili.com/video/av91748150?p=3&spm_id_from=pageDriver',3,'2022-02-28 00:46:17','2022-02-28 00:46:17');
/*!40000 ALTER TABLE `study_lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_progress`
--

DROP TABLE IF EXISTS `study_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `study_progress` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `hr_id` int NOT NULL COMMENT '进度添加者',
  `t_date` date NOT NULL COMMENT '记录的日期,不允许重复',
  `t_progress` int NOT NULL DEFAULT '0' COMMENT '进度数值',
  `remark` varchar(1000) NOT NULL DEFAULT '' COMMENT '备注',
  `last_update_time` datetime DEFAULT NULL COMMENT '最新修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_date` (`t_date`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_progress`
--

LOCK TABLES `study_progress` WRITE;
/*!40000 ALTER TABLE `study_progress` DISABLE KEYS */;
INSERT INTO `study_progress` (`id`, `hr_id`, `t_date`, `t_progress`, `remark`, `last_update_time`) VALUES (2,3,'2022-02-27',83,'今天做了课程管理，课题报告已经写完','2022-02-28 21:34:09'),(3,3,'2022-02-28',100,'今天做了文档管理、进度管理。','2022-02-28 20:29:32'),(6,3,'2022-02-26',20,'今天进度很少～～','2022-02-28 18:40:29'),(7,3,'2022-02-22',60,'一般般','2022-02-28 18:40:43'),(9,3,'2022-02-03',0,'okkkkkkk','2022-02-28 20:17:54'),(10,3,'2022-02-09',50,'SpringBOOOOOOOOOO!','2022-02-28 18:42:02'),(11,3,'2022-02-12',50,'Engureguo','2022-02-28 18:42:10'),(12,3,'2022-02-13',50,'Are you OK','2022-02-28 18:42:21'),(13,5,'2022-02-01',10,'libai:123','2022-02-28 20:17:58'),(15,5,'2022-02-14',100,'妈呀嘿嘿','2022-02-28 18:45:29'),(16,3,'2022-02-02',50,'222222','2022-02-28 18:57:40'),(17,3,'2022-02-25',10,'玩了一天，啥也没干','2022-02-28 21:34:52'),(18,3,'2022-02-24',30,'低效率～','2022-02-28 21:35:15'),(19,3,'2022-02-23',0,'外出游玩','2022-02-28 21:35:41'),(20,3,'2022-02-21',50,'76','2022-02-28 21:37:18'),(21,3,'2022-02-20',54,'','2022-02-28 21:37:25'),(23,3,'2022-02-19',25,'沃尔特沃尔特','2022-02-28 21:37:46'),(24,3,'2022-02-18',88,'发送到发送到噶三大嘎洒问题去·','2022-02-28 21:38:00'),(25,3,'2022-02-05',100,'阿斯顿噶三大','2022-02-28 21:39:30'),(26,3,'2022-03-01',90,'POI管理有进度了 ===> 下一步考虑将POI存入数据库，并在前端实现POI管理','2022-03-02 01:22:16');
/*!40000 ALTER TABLE `study_progress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sysmsg`
--

DROP TABLE IF EXISTS `sysmsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sysmsg` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mid` int DEFAULT NULL COMMENT '消息id',
  `type` int DEFAULT '0' COMMENT '0表示群发消息',
  `hrid` int DEFAULT NULL COMMENT '这条消息是给谁的',
  `state` int DEFAULT '0' COMMENT '0 未读 1 已读',
  PRIMARY KEY (`id`),
  KEY `hrid` (`hrid`),
  KEY `sysmsg_ibfk_1` (`mid`),
  CONSTRAINT `sysmsg_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `msgcontent` (`id`),
  CONSTRAINT `sysmsg_ibfk_2` FOREIGN KEY (`hrid`) REFERENCES `hr` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sysmsg`
--

LOCK TABLES `sysmsg` WRITE;
/*!40000 ALTER TABLE `sysmsg` DISABLE KEYS */;
INSERT INTO `sysmsg` (`id`, `mid`, `type`, `hrid`, `state`) VALUES (57,14,0,3,1),(58,14,0,5,1),(59,14,0,10,1),(60,14,0,11,0),(61,14,0,12,0),(62,15,0,3,1),(63,15,0,5,1),(64,15,0,10,1),(65,15,0,11,0),(66,15,0,12,0),(67,16,0,3,1),(68,16,0,5,1),(69,16,0,10,1),(70,16,0,11,0),(71,16,0,12,0),(72,17,0,3,1),(73,17,0,5,1),(74,17,0,10,1),(75,17,0,11,0),(76,17,0,12,0),(77,18,0,3,1),(78,18,0,5,0),(79,18,0,10,0),(80,18,0,11,0),(81,18,0,12,0);
/*!40000 ALTER TABLE `sysmsg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `three_d_models_type`
--

DROP TABLE IF EXISTS `three_d_models_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `three_d_models_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `type` varchar(50) NOT NULL COMMENT '类型',
  `desp` varchar(100) DEFAULT '' COMMENT '描述',
  `is_deleted` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `three_d_models_type`
--

LOCK TABLES `three_d_models_type` WRITE;
/*!40000 ALTER TABLE `three_d_models_type` DISABLE KEYS */;
INSERT INTO `three_d_models_type` (`id`, `type`, `desp`, `is_deleted`) VALUES (1,'json','json格式',0),(2,'obj','obj格式',0),(3,'obj_mtl','需要obj,mtl格式文件',0),(4,'fbx','fbx格式文件',1),(5,'stl','stl格式文件',0),(6,'dae','dae格式文件',0),(7,'ply','需要ply格式文件',0),(8,'gltf','需要gltf格式',0);
/*!40000 ALTER TABLE `three_d_models_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-07 17:08:09
